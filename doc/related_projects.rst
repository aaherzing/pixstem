.. _related_projects:

================
Related projects
================

- `fpd_live_imaging <https://fast_pixelated_detectors.gitlab.io/fpd_live_imaging/>`_: for doing live processing and imaging of data acquired on a fast pixelated detector using a scanning transmission electron microscope.
- `merlin_interface <https://fast_pixelated_detectors.gitlab.io/merlin_interface/>`_: Python library for interfacing with a Medipix3 detector through the Merlin software
- `pyXem <https://pyxem.github.io/pyxem/>`_: Python library for hybrid diffraction-imaging microscopy based on scanning electron diffraction (SED) data.
- `fpd <https://fpdpy.gitlab.io/fpd/>`_: Python library for analysing pixelated STEM Data, with a focus on STEM-DPC magnetic processing, STEM diffraction processing and Merlin file conversion.
